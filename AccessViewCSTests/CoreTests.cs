﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices.AccountManagement;
using AccessViewCS;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace AccessViewCS.Tests
{
    [TestClass()]
    public class CoreTests
    {
        [TestMethod()]
        public void GetPrincipalTest_Null()
        {
            // Expect null result from null input
            Principal result = Core.GetPrincipal(null);
            Assert.IsNull(result);
        }

        [TestMethod()]
        public void GetPrincipalTest_Empty()
        {
            // Expect null output from empty input
            Principal result = Core.GetPrincipal(string.Empty);
            Assert.IsNull(result);
        }

        [TestMethod()]
        public void GetPrincipalTest_Invalid()
        {
            // Expect null output from some garbage string
            Principal result = Core.GetPrincipal("\uffeff*\u008b{\ufa13\\", ContextType.Machine);
            Assert.IsNull(result);
        }

        /* Unusable on AppVeyor. Need a better searcher.
        [TestMethod()]
        public void GetPrincipalTest_Administrator()
        {
            // Expect a non-null Principal output from default local Administrator account
            Principal result = Core.GetPrincipal("Administrator", ContextType.Machine);
            Assert.IsNotNull(result);
            Assert.AreEqual("Administrator", result.SamAccountName);
        }
        */

        [TestMethod()]
        public void isUserTest_Null()
        {
            // Expect false from null input
            bool result = Core.isUser(null);
            Assert.IsFalse(result);
        }

        [TestMethod()]
        public void isUserTest_User()
        {
            // Expect true from local UserPrincipal input
            bool result = Core.isUser(new UserPrincipal(new PrincipalContext(ContextType.Machine)));
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void isUserTest_Group()
        {
            // Expect false from local GroupPrincipal input
            bool result = Core.isUser(new GroupPrincipal(new PrincipalContext(ContextType.Machine)));
            Assert.IsFalse(result);
        }

        [TestMethod()]
        public void isUserTest_Computer()
        {
            // Expect false from local ComputerPrincipal input
            bool result = Core.isUser(new ComputerPrincipal(new PrincipalContext(ContextType.Machine)));
            Assert.IsFalse(result);
        }

        [TestMethod()]
        public void isGroupTest_Null()
        {
            // Expect false from null input
            bool result = Core.isGroup(null);
            Assert.IsFalse(result);
        }

        [TestMethod()]
        public void isGroupTest_User()
        {
            // Expect false from local UserPrincipal input
            bool result = Core.isGroup(new UserPrincipal(new PrincipalContext(ContextType.Machine)));
            Assert.IsFalse(result);
        }

        [TestMethod()]
        public void isGroupTest_Group()
        {
            // Expect true from local GroupPrincipal input
            bool result = Core.isGroup(new GroupPrincipal(new PrincipalContext(ContextType.Machine)));
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void isGroupTest_Computer()
        {
            // Expect false from local ComputerPrincipal input
            bool result = Core.isGroup(new ComputerPrincipal(new PrincipalContext(ContextType.Machine)));
            Assert.IsFalse(result);
        }

        [TestMethod()]
        public void isComputerTest_Null()
        {
            // Expect false from null input
            bool result = Core.isComputer(null);
            Assert.IsFalse(result);
        }

        [TestMethod()]
        public void isComputerTest_User()
        {
            // Expect false from UserPrincipal input
            bool result = Core.isComputer(new UserPrincipal(new PrincipalContext(ContextType.Machine)));
            Assert.IsFalse(result);
        }

        [TestMethod()]
        public void isComputerTest_Group()
        {
            // Expect false from local GroupPrincipal input
            bool result = Core.isComputer(new GroupPrincipal(new PrincipalContext(ContextType.Machine)));
            Assert.IsFalse(result);
        }

        [TestMethod()]
        public void isComputerTest_Computer()
        {
            // Expect true from local ComputerPrincipal input
            bool result = Core.isComputer(new ComputerPrincipal(new PrincipalContext(ContextType.Machine)));
            Assert.IsTrue(result);
        }

        /* Unusable on AppVeyor. Need a better searcher.
        [TestMethod()]
        public void GetPrincipalMembershipTest_LocalAdmin()
        {
            // Local admin account should exist, and not give blank/null results
            List<string> result = Core.GetPrincipalMembership(Core.GetPrincipal("Administrator", ContextType.Machine));
            foreach (string each in result)
            {
                Assert.IsNotNull(each);
                Assert.AreNotEqual(string.Empty, each);
            }
        }
         */
    }
}
