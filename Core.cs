﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Diagnostics;
using System.Reflection;

namespace AccessViewCS
{
    public class Core
    {

        // Set attributes for easy querying
        private static AssemblyName asbly = Assembly.GetExecutingAssembly().GetName();
        public static string ReleaseTag = "-Alpha";
        public static string StatusTag = " INTERNAL";
        public static string Build = asbly.Version.ToString();
        public static string Version = String.Format("{0}.{1}", asbly.Version.Major.ToString(), asbly.Version.Minor.ToString());
        public static string Name = asbly.Name;

        private static string IdentityFormat = "{0}{1} v{2}{3}";
        public static string Identity = String.Format(IdentityFormat, Name, string.Empty, Version, ReleaseTag);
        public static string FullIdentity = String.Format(IdentityFormat, Name, StatusTag, Build, ReleaseTag);


        // GetPrincipal: Returns base Principal object from name search, or null if no matches
        public static Principal GetPrincipal(string subjectName, ContextType subjectContext = ContextType.Domain)
        {
            if (subjectName == null || subjectName == string.Empty)
            {
                return null;
            }
            Principal result = Principal.FindByIdentity(new PrincipalContext(subjectContext), subjectName);
            return result;
        }

        // Three quick tests to check if User/Group/Computer
        public static bool isUser(Principal subject)
        {
            if (subject == null)
                return false;

            return (subject.GetType() == typeof(UserPrincipal));
        }

        public static bool isGroup(Principal subject)
        {
            if (subject == null)
                return false;

            return (subject.GetType() == typeof(GroupPrincipal));
        }

        public static bool isComputer(Principal subject)
        {
            if (subject == null)
                return false;

            return (subject.GetType() == typeof(ComputerPrincipal));
        }


        // GetPrincipalMembership: Returns List<string> containing group SamAccountName of all groups Principal belongs to
        public static List<string> GetPrincipalMembership(Principal subject)
        {
            List<string> grouplist = new List<string>();
            foreach (GroupPrincipal group in subject.GetGroups())
            {
                grouplist.Add(group.SamAccountName);
            }
            grouplist.Sort();
            return grouplist;
        }

        public enum PrincipalType
        {
            None, User, Group, Computer
        }

        public static void doLabelLink(LinkLabel sender, LinkLabelLinkClickedEventArgs e)
        {
            string url;
            if (e.Link.LinkData != null)
                url = e.Link.LinkData.ToString();
            else
                url = sender.Text.Substring(e.Link.Start, e.Link.Length);

            if (!url.Contains("://"))
                url = "http://" + url;

            var si = new ProcessStartInfo(url);
            Process.Start(si);
            sender.LinkVisited = true;
        }

    }
}
