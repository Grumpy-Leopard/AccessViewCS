﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace AccessViewCS
{
    class LogonScript
    {
        // Public items
        public List<String> driveMappings = new List<String>();

        public List<String> scriptContent = new List<String>();
        private string fileName;
        private string logonServer = @"%LOGONSERVER%";
        private string userName = @"%USERNAME%";

        public LogonScript(string file)
        {
            fileName = file;
            Load();
        }

        public LogonScript(string file, string server)
        {
            fileName = file;
            logonServer = server;
            Load();
        }

        public LogonScript(string file, string server, string user)
        {
            fileName = file;
            logonServer = server;
            userName = user;
            Load();
        }

        private void Load()
        {
            // Putting all file load operations in background threads.
            var threading = new CountdownEvent(1);

            threading.AddCount();
            Task.Factory.StartNew(() => ReadFile(fileName, threading));
            threading.Signal();
            threading.Wait();

            threading.Reset();

            threading.AddCount();
            Task.Factory.StartNew(() => getDriveMappings(threading));
            threading.Signal();
            threading.Wait();
        }

        // Overload to allow Async processing.
        private void ReadFile(string file, CountdownEvent e)
        {
            ReadFile(file);
            e.Signal();
        }

        private void ReadFile(string file)
        {
            string line = String.Empty;
            if (File.Exists(file))
            {
                TextReader tr = File.OpenText(file);

                while (null != (line = tr.ReadLine()))                    // Force uppercase for easier parsing
                {
                    line = line.ToUpper().Trim();
                    // Check for uncommented includes
                    if (line.Contains("CALL ") && (false == line.StartsWith("REM ")))
                    {
                        line = line.Replace("CALL", "?")                            // Replace with invalid char for easy finding
                            .Substring(line.LastIndexOf("?")+1)                     // Substring from that to the end
                            .Replace("?",String.Empty)                              // Remove the ?
                            .Trim()                                                 // Clear whitespace
                            .Replace(@"%LOGONSERVER%", logonServer)                 // Replace logonserver variable
                            .Replace(@"%USERNAME%", userName);                      // Replace username variable

                        ReadFile(line);
                   }
                   else
                        scriptContent.Add(line);
                }

            }
        }

        // Overload to allow async processing.
        private void getDriveMappings(CountdownEvent e)
        {
            getDriveMappings();
            e.Signal();
        }

        private void getDriveMappings()
        {
            foreach (string line in scriptContent)
            {
                if (line.Trim().StartsWith("NET USE") && (line.IndexOf(":") > 0))
                {
                    string tmp = line;
                    if (tmp.IndexOf(">") > 0)
                        tmp = tmp.Substring(0, tmp.IndexOf(">"));
                    tmp = tmp.Replace("NET USE ", String.Empty)
                        .Replace("/PERSISTENT:NO", String.Empty)
                        .Trim();
                    if (String.Empty != tmp)
                        driveMappings.Add(tmp);
                }
            }
            driveMappings.Sort();
        }
    }
}
