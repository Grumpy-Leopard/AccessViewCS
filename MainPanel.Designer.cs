﻿namespace AccessViewCS
{
    partial class MainPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainPanel));
            this.GoButton = new System.Windows.Forms.Button();
            this.TypeIndicator = new System.Windows.Forms.Label();
            this.MainTabs = new System.Windows.Forms.TabControl();
            this.Tab_About = new System.Windows.Forms.TabPage();
            this.CreditsButton = new System.Windows.Forms.Button();
            this.LicenseButton = new System.Windows.Forms.Button();
            this.IssueTrackerLink1 = new System.Windows.Forms.LinkLabel();
            this.AppVersionLabel = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.Tab_User1 = new System.Windows.Forms.TabPage();
            this.OpenLogonScript = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.LogonScript = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Expiry = new System.Windows.Forms.TextBox();
            this.NotDisabled = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LogonTo = new System.Windows.Forms.ListBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LockoutNum = new System.Windows.Forms.TextBox();
            this.LastBad = new System.Windows.Forms.TextBox();
            this.Lockout = new System.Windows.Forms.TextBox();
            this.PWChange = new System.Windows.Forms.TextBox();
            this.LastLogon = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.UPN = new System.Windows.Forms.TextBox();
            this.Fullname = new System.Windows.Forms.TextBox();
            this.Comment = new System.Windows.Forms.TextBox();
            this.DistinguishedName = new System.Windows.Forms.TextBox();
            this.Username = new System.Windows.Forms.TextBox();
            this.Tab_User2 = new System.Windows.Forms.TabPage();
            this.GroupLookup = new System.Windows.Forms.Button();
            this.GroupsList = new System.Windows.Forms.ListBox();
            this.Tab_User3 = new System.Windows.Forms.TabPage();
            this.DriveMappings = new System.Windows.Forms.ListBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Homepath = new System.Windows.Forms.TextBox();
            this.Homedrive = new System.Windows.Forms.TextBox();
            this.Tab_Computer1 = new System.Windows.Forms.TabPage();
            this.Tab_WIP = new System.Windows.Forms.TabPage();
            this.IssueTrackerLinkWIP = new System.Windows.Forms.LinkLabel();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Tab_Text = new System.Windows.Forms.TabPage();
            this.TextDisplay = new System.Windows.Forms.TextBox();
            this.OpenSearch = new System.Windows.Forms.Button();
            this.MainInput = new System.Windows.Forms.ComboBox();
            this.MainTabs.SuspendLayout();
            this.Tab_About.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.Tab_User1.SuspendLayout();
            this.Tab_User2.SuspendLayout();
            this.Tab_User3.SuspendLayout();
            this.Tab_WIP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.Tab_Text.SuspendLayout();
            this.SuspendLayout();
            // 
            // GoButton
            // 
            this.GoButton.Location = new System.Drawing.Point(281, 12);
            this.GoButton.Name = "GoButton";
            this.GoButton.Size = new System.Drawing.Size(75, 23);
            this.GoButton.TabIndex = 2;
            this.GoButton.Text = "Go";
            this.GoButton.UseVisualStyleBackColor = true;
            this.GoButton.Click += new System.EventHandler(this.GoButton_Click);
            // 
            // TypeIndicator
            // 
            this.TypeIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TypeIndicator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TypeIndicator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TypeIndicator.Location = new System.Drawing.Point(434, 9);
            this.TypeIndicator.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TypeIndicator.Name = "TypeIndicator";
            this.TypeIndicator.Size = new System.Drawing.Size(133, 28);
            this.TypeIndicator.TabIndex = 0;
            this.TypeIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainTabs
            // 
            this.MainTabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainTabs.Controls.Add(this.Tab_About);
            this.MainTabs.Controls.Add(this.Tab_User1);
            this.MainTabs.Controls.Add(this.Tab_User2);
            this.MainTabs.Controls.Add(this.Tab_User3);
            this.MainTabs.Controls.Add(this.Tab_Computer1);
            this.MainTabs.Controls.Add(this.Tab_WIP);
            this.MainTabs.Controls.Add(this.Tab_Text);
            this.MainTabs.Location = new System.Drawing.Point(12, 55);
            this.MainTabs.Name = "MainTabs";
            this.MainTabs.SelectedIndex = 0;
            this.MainTabs.Size = new System.Drawing.Size(556, 438);
            this.MainTabs.TabIndex = 3;
            // 
            // Tab_About
            // 
            this.Tab_About.Controls.Add(this.CreditsButton);
            this.Tab_About.Controls.Add(this.LicenseButton);
            this.Tab_About.Controls.Add(this.IssueTrackerLink1);
            this.Tab_About.Controls.Add(this.AppVersionLabel);
            this.Tab_About.Controls.Add(this.label15);
            this.Tab_About.Controls.Add(this.pictureBox1);
            this.Tab_About.Controls.Add(this.label14);
            this.Tab_About.Location = new System.Drawing.Point(4, 25);
            this.Tab_About.Name = "Tab_About";
            this.Tab_About.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Tab_About.Size = new System.Drawing.Size(548, 409);
            this.Tab_About.TabIndex = 3;
            this.Tab_About.Text = "About";
            this.Tab_About.UseVisualStyleBackColor = true;
            // 
            // CreditsButton
            // 
            this.CreditsButton.Location = new System.Drawing.Point(65, 135);
            this.CreditsButton.Name = "CreditsButton";
            this.CreditsButton.Size = new System.Drawing.Size(75, 23);
            this.CreditsButton.TabIndex = 7;
            this.CreditsButton.Text = "Credits";
            this.CreditsButton.UseVisualStyleBackColor = true;
            this.CreditsButton.Click += new System.EventHandler(this.CreditsButton_Click);
            // 
            // LicenseButton
            // 
            this.LicenseButton.Location = new System.Drawing.Point(65, 106);
            this.LicenseButton.Name = "LicenseButton";
            this.LicenseButton.Size = new System.Drawing.Size(75, 23);
            this.LicenseButton.TabIndex = 6;
            this.LicenseButton.Text = "License";
            this.LicenseButton.UseVisualStyleBackColor = true;
            this.LicenseButton.Click += new System.EventHandler(this.LicenseButton_Click);
            // 
            // IssueTrackerLink1
            // 
            this.IssueTrackerLink1.AutoSize = true;
            this.IssueTrackerLink1.Location = new System.Drawing.Point(104, 331);
            this.IssueTrackerLink1.Name = "IssueTrackerLink1";
            this.IssueTrackerLink1.Size = new System.Drawing.Size(312, 16);
            this.IssueTrackerLink1.TabIndex = 5;
            this.IssueTrackerLink1.TabStop = true;
            this.IssueTrackerLink1.Text = "https://gitlab.com/yukienterprises/AccessViewCS";
            this.IssueTrackerLink1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.IssueTrackerLink1_LinkClicked);
            // 
            // AppVersionLabel
            // 
            this.AppVersionLabel.AutoSize = true;
            this.AppVersionLabel.Location = new System.Drawing.Point(62, 274);
            this.AppVersionLabel.Name = "AppVersionLabel";
            this.AppVersionLabel.Size = new System.Drawing.Size(130, 16);
            this.AppVersionLabel.TabIndex = 3;
            this.AppVersionLabel.Text = "Application Version: ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(30, 32);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(194, 16);
            this.label15.TabIndex = 2;
            this.label15.Text = "Developed by Yuki Enterprises";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::AccessViewCS.Properties.Resources.AvatarLogo;
            this.pictureBox1.Location = new System.Drawing.Point(334, 14);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 200);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(131, 48);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(107, 16);
            this.label14.TabIndex = 0;
            this.label14.Text = "Copyright © 2014";
            // 
            // Tab_User1
            // 
            this.Tab_User1.Controls.Add(this.OpenLogonScript);
            this.Tab_User1.Controls.Add(this.label13);
            this.Tab_User1.Controls.Add(this.LogonScript);
            this.Tab_User1.Controls.Add(this.label11);
            this.Tab_User1.Controls.Add(this.Expiry);
            this.Tab_User1.Controls.Add(this.NotDisabled);
            this.Tab_User1.Controls.Add(this.label8);
            this.Tab_User1.Controls.Add(this.LogonTo);
            this.Tab_User1.Controls.Add(this.label9);
            this.Tab_User1.Controls.Add(this.label7);
            this.Tab_User1.Controls.Add(this.label6);
            this.Tab_User1.Controls.Add(this.LockoutNum);
            this.Tab_User1.Controls.Add(this.LastBad);
            this.Tab_User1.Controls.Add(this.Lockout);
            this.Tab_User1.Controls.Add(this.PWChange);
            this.Tab_User1.Controls.Add(this.LastLogon);
            this.Tab_User1.Controls.Add(this.label5);
            this.Tab_User1.Controls.Add(this.label4);
            this.Tab_User1.Controls.Add(this.label3);
            this.Tab_User1.Controls.Add(this.label2);
            this.Tab_User1.Controls.Add(this.label1);
            this.Tab_User1.Controls.Add(this.UPN);
            this.Tab_User1.Controls.Add(this.Fullname);
            this.Tab_User1.Controls.Add(this.Comment);
            this.Tab_User1.Controls.Add(this.DistinguishedName);
            this.Tab_User1.Controls.Add(this.Username);
            this.Tab_User1.Location = new System.Drawing.Point(4, 25);
            this.Tab_User1.Name = "Tab_User1";
            this.Tab_User1.Padding = new System.Windows.Forms.Padding(3);
            this.Tab_User1.Size = new System.Drawing.Size(548, 409);
            this.Tab_User1.TabIndex = 0;
            this.Tab_User1.Text = "User";
            this.Tab_User1.UseVisualStyleBackColor = true;
            // 
            // OpenLogonScript
            // 
            this.OpenLogonScript.Location = new System.Drawing.Point(505, 123);
            this.OpenLogonScript.Name = "OpenLogonScript";
            this.OpenLogonScript.Size = new System.Drawing.Size(29, 23);
            this.OpenLogonScript.TabIndex = 32;
            this.OpenLogonScript.Text = "...";
            this.OpenLogonScript.UseVisualStyleBackColor = true;
            this.OpenLogonScript.Click += new System.EventHandler(this.OpenLogonScript_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(18, 126);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 16);
            this.label13.TabIndex = 31;
            this.label13.Text = "Logon Script";
            // 
            // LogonScript
            // 
            this.LogonScript.Location = new System.Drawing.Point(111, 123);
            this.LogonScript.Name = "LogonScript";
            this.LogonScript.ReadOnly = true;
            this.LogonScript.Size = new System.Drawing.Size(388, 22);
            this.LogonScript.TabIndex = 30;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 210);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 16);
            this.label11.TabIndex = 29;
            this.label11.Text = "Expiry";
            // 
            // Expiry
            // 
            this.Expiry.Location = new System.Drawing.Point(111, 207);
            this.Expiry.Name = "Expiry";
            this.Expiry.ReadOnly = true;
            this.Expiry.Size = new System.Drawing.Size(148, 22);
            this.Expiry.TabIndex = 28;
            // 
            // NotDisabled
            // 
            this.NotDisabled.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.NotDisabled.BackColor = System.Drawing.Color.Silver;
            this.NotDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NotDisabled.Location = new System.Drawing.Point(87, 299);
            this.NotDisabled.Name = "NotDisabled";
            this.NotDisabled.Size = new System.Drawing.Size(172, 73);
            this.NotDisabled.TabIndex = 27;
            this.NotDisabled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(368, 240);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 16);
            this.label8.TabIndex = 26;
            this.label8.Text = "Allowed Logon To:";
            // 
            // LogonTo
            // 
            this.LogonTo.FormattingEnabled = true;
            this.LogonTo.ItemHeight = 16;
            this.LogonTo.Location = new System.Drawing.Point(371, 259);
            this.LogonTo.Name = "LogonTo";
            this.LogonTo.Size = new System.Drawing.Size(163, 132);
            this.LogonTo.Sorted = true;
            this.LogonTo.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(304, 182);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 16);
            this.label9.TabIndex = 24;
            this.label9.Text = "Last Bad";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 182);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 16);
            this.label7.TabIndex = 22;
            this.label7.Text = "Lockout";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(277, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 16);
            this.label6.TabIndex = 21;
            this.label6.Text = "PW Changed";
            // 
            // LockoutNum
            // 
            this.LockoutNum.Location = new System.Drawing.Point(265, 179);
            this.LockoutNum.Name = "LockoutNum";
            this.LockoutNum.ReadOnly = true;
            this.LockoutNum.Size = new System.Drawing.Size(25, 22);
            this.LockoutNum.TabIndex = 20;
            // 
            // LastBad
            // 
            this.LastBad.Location = new System.Drawing.Point(371, 179);
            this.LastBad.Name = "LastBad";
            this.LastBad.ReadOnly = true;
            this.LastBad.Size = new System.Drawing.Size(163, 22);
            this.LastBad.TabIndex = 19;
            // 
            // Lockout
            // 
            this.Lockout.Location = new System.Drawing.Point(111, 179);
            this.Lockout.Name = "Lockout";
            this.Lockout.ReadOnly = true;
            this.Lockout.Size = new System.Drawing.Size(148, 22);
            this.Lockout.TabIndex = 18;
            // 
            // PWChange
            // 
            this.PWChange.Location = new System.Drawing.Point(371, 151);
            this.PWChange.Name = "PWChange";
            this.PWChange.ReadOnly = true;
            this.PWChange.Size = new System.Drawing.Size(163, 22);
            this.PWChange.TabIndex = 17;
            // 
            // LastLogon
            // 
            this.LastLogon.Location = new System.Drawing.Point(111, 151);
            this.LastLogon.Name = "LastLogon";
            this.LastLogon.ReadOnly = true;
            this.LastLogon.Size = new System.Drawing.Size(148, 22);
            this.LastLogon.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 16);
            this.label5.TabIndex = 15;
            this.label5.Text = "Last Logon";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 16);
            this.label4.TabIndex = 14;
            this.label4.Text = "LDAP DN";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 16);
            this.label3.TabIndex = 13;
            this.label3.Text = "Comment";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 16);
            this.label2.TabIndex = 12;
            this.label2.Text = "Full Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 16);
            this.label1.TabIndex = 11;
            this.label1.Text = "Username";
            // 
            // UPN
            // 
            this.UPN.Location = new System.Drawing.Point(230, 11);
            this.UPN.Name = "UPN";
            this.UPN.ReadOnly = true;
            this.UPN.Size = new System.Drawing.Size(304, 22);
            this.UPN.TabIndex = 10;
            // 
            // Fullname
            // 
            this.Fullname.Location = new System.Drawing.Point(111, 39);
            this.Fullname.Name = "Fullname";
            this.Fullname.ReadOnly = true;
            this.Fullname.Size = new System.Drawing.Size(423, 22);
            this.Fullname.TabIndex = 9;
            // 
            // Comment
            // 
            this.Comment.Location = new System.Drawing.Point(111, 67);
            this.Comment.Name = "Comment";
            this.Comment.ReadOnly = true;
            this.Comment.Size = new System.Drawing.Size(423, 22);
            this.Comment.TabIndex = 8;
            // 
            // DistinguishedName
            // 
            this.DistinguishedName.Location = new System.Drawing.Point(111, 95);
            this.DistinguishedName.Name = "DistinguishedName";
            this.DistinguishedName.ReadOnly = true;
            this.DistinguishedName.Size = new System.Drawing.Size(423, 22);
            this.DistinguishedName.TabIndex = 7;
            // 
            // Username
            // 
            this.Username.Location = new System.Drawing.Point(111, 11);
            this.Username.Name = "Username";
            this.Username.ReadOnly = true;
            this.Username.Size = new System.Drawing.Size(113, 22);
            this.Username.TabIndex = 6;
            // 
            // Tab_User2
            // 
            this.Tab_User2.Controls.Add(this.GroupLookup);
            this.Tab_User2.Controls.Add(this.GroupsList);
            this.Tab_User2.Location = new System.Drawing.Point(4, 25);
            this.Tab_User2.Name = "Tab_User2";
            this.Tab_User2.Padding = new System.Windows.Forms.Padding(3);
            this.Tab_User2.Size = new System.Drawing.Size(548, 409);
            this.Tab_User2.TabIndex = 1;
            this.Tab_User2.Text = "Groups";
            this.Tab_User2.UseVisualStyleBackColor = true;
            // 
            // GroupLookup
            // 
            this.GroupLookup.Location = new System.Drawing.Point(467, 380);
            this.GroupLookup.Name = "GroupLookup";
            this.GroupLookup.Size = new System.Drawing.Size(75, 23);
            this.GroupLookup.TabIndex = 1;
            this.GroupLookup.Text = "Lookup";
            this.GroupLookup.UseVisualStyleBackColor = true;
            this.GroupLookup.Click += new System.EventHandler(this.GroupLookup_Click);
            // 
            // GroupsList
            // 
            this.GroupsList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupsList.FormattingEnabled = true;
            this.GroupsList.ItemHeight = 16;
            this.GroupsList.Location = new System.Drawing.Point(6, 6);
            this.GroupsList.Name = "GroupsList";
            this.GroupsList.Size = new System.Drawing.Size(536, 372);
            this.GroupsList.TabIndex = 0;
            // 
            // Tab_User3
            // 
            this.Tab_User3.Controls.Add(this.DriveMappings);
            this.Tab_User3.Controls.Add(this.label12);
            this.Tab_User3.Controls.Add(this.label10);
            this.Tab_User3.Controls.Add(this.Homepath);
            this.Tab_User3.Controls.Add(this.Homedrive);
            this.Tab_User3.Location = new System.Drawing.Point(4, 25);
            this.Tab_User3.Name = "Tab_User3";
            this.Tab_User3.Size = new System.Drawing.Size(548, 409);
            this.Tab_User3.TabIndex = 4;
            this.Tab_User3.Text = "Drives";
            this.Tab_User3.UseVisualStyleBackColor = true;
            // 
            // DriveMappings
            // 
            this.DriveMappings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DriveMappings.FormattingEnabled = true;
            this.DriveMappings.ItemHeight = 16;
            this.DriveMappings.Location = new System.Drawing.Point(111, 42);
            this.DriveMappings.Name = "DriveMappings";
            this.DriveMappings.Size = new System.Drawing.Size(423, 340);
            this.DriveMappings.TabIndex = 16;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(25, 42);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 16);
            this.label12.TabIndex = 15;
            this.label12.Text = "Mappings";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(18, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 16);
            this.label10.TabIndex = 14;
            this.label10.Text = "Homedrive";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Homepath
            // 
            this.Homepath.Location = new System.Drawing.Point(230, 11);
            this.Homepath.Name = "Homepath";
            this.Homepath.ReadOnly = true;
            this.Homepath.Size = new System.Drawing.Size(304, 22);
            this.Homepath.TabIndex = 13;
            // 
            // Homedrive
            // 
            this.Homedrive.Location = new System.Drawing.Point(111, 11);
            this.Homedrive.Name = "Homedrive";
            this.Homedrive.ReadOnly = true;
            this.Homedrive.Size = new System.Drawing.Size(113, 22);
            this.Homedrive.TabIndex = 12;
            // 
            // Tab_Computer1
            // 
            this.Tab_Computer1.Location = new System.Drawing.Point(4, 25);
            this.Tab_Computer1.Name = "Tab_Computer1";
            this.Tab_Computer1.Size = new System.Drawing.Size(548, 409);
            this.Tab_Computer1.TabIndex = 2;
            this.Tab_Computer1.Text = "Computer";
            this.Tab_Computer1.UseVisualStyleBackColor = true;
            // 
            // Tab_WIP
            // 
            this.Tab_WIP.Controls.Add(this.IssueTrackerLinkWIP);
            this.Tab_WIP.Controls.Add(this.label17);
            this.Tab_WIP.Controls.Add(this.label16);
            this.Tab_WIP.Controls.Add(this.pictureBox2);
            this.Tab_WIP.Location = new System.Drawing.Point(4, 25);
            this.Tab_WIP.Name = "Tab_WIP";
            this.Tab_WIP.Size = new System.Drawing.Size(548, 409);
            this.Tab_WIP.TabIndex = 5;
            this.Tab_WIP.Text = "WIP";
            this.Tab_WIP.UseVisualStyleBackColor = true;
            // 
            // IssueTrackerLinkWIP
            // 
            this.IssueTrackerLinkWIP.AutoSize = true;
            this.IssueTrackerLinkWIP.Location = new System.Drawing.Point(102, 333);
            this.IssueTrackerLinkWIP.Name = "IssueTrackerLinkWIP";
            this.IssueTrackerLinkWIP.Size = new System.Drawing.Size(351, 16);
            this.IssueTrackerLinkWIP.TabIndex = 3;
            this.IssueTrackerLinkWIP.TabStop = true;
            this.IssueTrackerLinkWIP.Text = "https://gitlab.com/yukienterprises/AccessViewCS/issues";
            this.IssueTrackerLinkWIP.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.IssueTrackerLinkWIP_LinkClicked);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(58, 308);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(441, 16);
            this.label17.TabIndex = 2;
            this.label17.Text = "Please check again in a future build or log a request on the Issue Tracker.";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(111, 282);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(338, 16);
            this.label16.TabIndex = 1;
            this.label16.Text = "The feature you have selected is not currently available.";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::AccessViewCS.Properties.Resources.WIP;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(128, 13);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(302, 252);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // Tab_Text
            // 
            this.Tab_Text.Controls.Add(this.TextDisplay);
            this.Tab_Text.Location = new System.Drawing.Point(4, 25);
            this.Tab_Text.Name = "Tab_Text";
            this.Tab_Text.Size = new System.Drawing.Size(548, 409);
            this.Tab_Text.TabIndex = 6;
            this.Tab_Text.Text = "Text";
            this.Tab_Text.UseVisualStyleBackColor = true;
            // 
            // TextDisplay
            // 
            this.TextDisplay.Location = new System.Drawing.Point(3, 3);
            this.TextDisplay.Multiline = true;
            this.TextDisplay.Name = "TextDisplay";
            this.TextDisplay.ReadOnly = true;
            this.TextDisplay.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextDisplay.Size = new System.Drawing.Size(542, 403);
            this.TextDisplay.TabIndex = 0;
            // 
            // OpenSearch
            // 
            this.OpenSearch.Location = new System.Drawing.Point(246, 12);
            this.OpenSearch.Name = "OpenSearch";
            this.OpenSearch.Size = new System.Drawing.Size(29, 23);
            this.OpenSearch.TabIndex = 33;
            this.OpenSearch.Text = "...";
            this.OpenSearch.UseVisualStyleBackColor = true;
            this.OpenSearch.Click += new System.EventHandler(this.OpenSearch_Click);
            // 
            // MainInput
            // 
            this.MainInput.FormattingEnabled = true;
            this.MainInput.Location = new System.Drawing.Point(12, 13);
            this.MainInput.Name = "MainInput";
            this.MainInput.Size = new System.Drawing.Size(228, 24);
            this.MainInput.TabIndex = 2;
            this.MainInput.SelectedIndexChanged += new System.EventHandler(this.MainInput_SelectedIndexChanged);
            // 
            // MainPanel
            // 
            this.AcceptButton = this.GoButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 505);
            this.Controls.Add(this.MainInput);
            this.Controls.Add(this.OpenSearch);
            this.Controls.Add(this.MainTabs);
            this.Controls.Add(this.GoButton);
            this.Controls.Add(this.TypeIndicator);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainPanel";
            this.Text = "AccessViewCS";
            this.MainTabs.ResumeLayout(false);
            this.Tab_About.ResumeLayout(false);
            this.Tab_About.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.Tab_User1.ResumeLayout(false);
            this.Tab_User1.PerformLayout();
            this.Tab_User2.ResumeLayout(false);
            this.Tab_User3.ResumeLayout(false);
            this.Tab_User3.PerformLayout();
            this.Tab_WIP.ResumeLayout(false);
            this.Tab_WIP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.Tab_Text.ResumeLayout(false);
            this.Tab_Text.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button GoButton;
        private System.Windows.Forms.Label TypeIndicator;
        private System.Windows.Forms.TabControl MainTabs;
        private System.Windows.Forms.TabPage Tab_User1;
        private System.Windows.Forms.TabPage Tab_User2;
        private System.Windows.Forms.TabPage Tab_Computer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox UPN;
        private System.Windows.Forms.TextBox Fullname;
        private System.Windows.Forms.TextBox Comment;
        private System.Windows.Forms.TextBox DistinguishedName;
        private System.Windows.Forms.TextBox Username;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage Tab_About;
        private System.Windows.Forms.ListBox GroupsList;
        private System.Windows.Forms.TabPage Tab_User3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox LockoutNum;
        private System.Windows.Forms.TextBox LastBad;
        private System.Windows.Forms.TextBox Lockout;
        private System.Windows.Forms.TextBox PWChange;
        private System.Windows.Forms.TextBox LastLogon;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListBox LogonTo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Expiry;
        private System.Windows.Forms.Label NotDisabled;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Homepath;
        private System.Windows.Forms.TextBox Homedrive;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox LogonScript;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label AppVersionLabel;
        private System.Windows.Forms.Button OpenLogonScript;
        private System.Windows.Forms.TabPage Tab_WIP;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.LinkLabel IssueTrackerLinkWIP;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.LinkLabel IssueTrackerLink1;
        private System.Windows.Forms.Button OpenSearch;
        private System.Windows.Forms.Button CreditsButton;
        private System.Windows.Forms.Button LicenseButton;
        private System.Windows.Forms.TabPage Tab_Text;
        private System.Windows.Forms.TextBox TextDisplay;
        private System.Windows.Forms.ListBox DriveMappings;
        private System.Windows.Forms.Button GroupLookup;
        private System.Windows.Forms.ComboBox MainInput;
    }
}