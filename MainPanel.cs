﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Threading;
using System.Threading.Tasks;

namespace AccessViewCS
{
    public partial class MainPanel : Form
    {
        private BindingList<String> History = new BindingList<String>();

        public MainPanel()
        {
            InitializeComponent();

            // Hide all tabs except the About box on load
            hideAllTabs();

            // Fix version number on About tab
            AppVersionLabel.Text = Core.FullIdentity;
            this.Text = Core.Identity;

            // Set up history
            MainInput.DataSource = History;
            History.Add(String.Empty);
        }

        private void AddHistory(string entry)
        {
            // So we can sort/modify/etc in the future if needed
            if (!History.Contains(entry))
            {
                History.Add(entry);
            }
        }

        private void GoButton_Click(object sender, EventArgs e)
        {
            doSearch(MainInput.Text);
        }

        private void doSearch(string input)
        {
            doStuff(Core.GetPrincipal(input));
        }
        private void doStuff(Principal subjectraw)
        {
            switch (checkType(subjectraw))
            {
                case Core.PrincipalType.User:
                    showUserTabs();
                    UserPrincipal user = (subjectraw as UserPrincipal);
                    AddHistory(user.SamAccountName);

                    Username.Text = user.SamAccountName;
                    UPN.Text = user.UserPrincipalName;
                    Fullname.Text = user.DisplayName;
                    Comment.Text = user.Description;
                    DistinguishedName.Text = user.DistinguishedName;
                    LogonScript.Text = user.ScriptPath;

                    LastLogon.Text = user.LastLogon.ToString();
                    PWChange.Text = user.LastPasswordSet.ToString();

                    Lockout.Text = user.AccountLockoutTime.ToString();
                    if (String.Empty != user.AccountLockoutTime.ToString())
                        Lockout.BackColor = Color.PaleVioletRed;

                    LockoutNum.Text = user.BadLogonCount.ToString();
                    LastBad.Text = user.LastBadPasswordAttempt.ToString();

                    Expiry.Text = user.AccountExpirationDate.ToString();
                    if (user.AccountExpirationDate <= DateTime.Now)
                        Expiry.BackColor = Color.PaleVioletRed;

                    // Have to force type as Enabled is nullable
                    if ((bool)user.Enabled)
                    {
                        NotDisabled.Text = "Enabled";
                        NotDisabled.BackColor = Color.PaleGreen;
                    }
                    else
                    {
                        NotDisabled.Text = "Disabled";
                        NotDisabled.BackColor = Color.PaleVioletRed;
                    }

                    LogonTo.DataSource = user.PermittedWorkstations;

                    GroupsList.DataSource = Core.GetPrincipalMembership(user);

                    Homedrive.Text = user.HomeDrive;
                    Homepath.Text = user.HomeDirectory;

                    string domainController = (new PrincipalContext(ContextType.Domain)).ConnectedServer;
                    string logonPath = String.Format(@"\\{0}\NETLOGON\", domainController);

                    var userscript = new LogonScript(logonPath + user.ScriptPath, domainController, user.SamAccountName);

                    DriveMappings.DataSource = userscript.driveMappings;

                    break;
                case Core.PrincipalType.Group:
                    GroupPrincipal group = subjectraw as GroupPrincipal;
                    AddHistory(group.SamAccountName);
                    showGroupTabs();
                    break;
                case Core.PrincipalType.Computer:
                    AddHistory(subjectraw.DisplayName);
                    showComputerTabs();
                    break;
                default:
                    hideAllTabs();
                    break;
            }

            MainInput.Focus();
            MainInput.Text = String.Empty;
        }

        private Core.PrincipalType checkType(Principal subject)
        {
            // TODO: Need to refactor with assertions
            if (subject == null)
            {
                TypeIndicator.Text = "ERROR";
                TypeIndicator.ForeColor = Color.Red;
                return Core.PrincipalType.None;
            }
            if (Core.isUser(subject))
            {
                TypeIndicator.Text = "User";
                TypeIndicator.ForeColor = Color.Blue;
                return Core.PrincipalType.User;
            }
            if (Core.isGroup(subject))
            {
                TypeIndicator.Text = "Group";
                TypeIndicator.ForeColor = Color.Blue;
                return Core.PrincipalType.Group;
            }
            if (Core.isComputer(subject))
            {
                TypeIndicator.Text = "Computer";
                TypeIndicator.ForeColor = Color.Blue;
                return Core.PrincipalType.Computer;
            }
            TypeIndicator.Text = "ERROR";
            TypeIndicator.ForeColor = Color.Red;
            return Core.PrincipalType.None;
        }

        // Hide all tabs except the About box
        private void hideAllTabs()
        {
            foreach (TabPage tabpage in MainTabs.TabPages)
            {
                if (tabpage != Tab_About)
                {
                    MainTabs.TabPages.Remove(tabpage);
                }
            }

        }
        private void showUserTabs()
        {
            hideAllTabs();
            MainTabs.TabPages.Add(Tab_User1);
            MainTabs.TabPages.Add(Tab_User2);
            MainTabs.TabPages.Add(Tab_User3);
            MainTabs.SelectedTab = Tab_User1;
        }
        private void showGroupTabs()
        {
            hideAllTabs();
            MainTabs.TabPages.Add(Tab_WIP);
            MainTabs.SelectedTab = Tab_WIP;
        }
        private void showComputerTabs()
        {
            hideAllTabs();
            MainTabs.TabPages.Add(Tab_WIP);
            MainTabs.SelectedTab = Tab_WIP;
        }

        private void showTextTab(string text, string title = null)
        {
            hideAllTabs();
            TextDisplay.Text = text;
            if (null != title)
            {
                Tab_Text.Text = title;
            }
            MainTabs.TabPages.Add(Tab_Text);
            MainTabs.SelectedTab = Tab_Text;
            TextDisplay.Select(0, 0);
        }


        private void OpenLogonScript_Click(object sender, EventArgs e)
        {
            string domainController = (new PrincipalContext(ContextType.Domain)).ConnectedServer;
            string logonPath = String.Format(@"\\{0}\NETLOGON\", domainController);

            // Hardcodes notepad, because it's guaranteed to be available on Windows
            // Could possibly use own text control, but can't use file association as these are "executable" files
            System.Diagnostics.Process.Start("notepad.exe", logonPath + LogonScript.Text);
        }

        private void IssueTrackerLinkWIP_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Core.doLabelLink(IssueTrackerLinkWIP, e);
        }

        private void IssueTrackerLink1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Core.doLabelLink(IssueTrackerLink1, e);
        }

        private void OpenSearch_Click(object sender, EventArgs e)
        {
            // TODO: Not yet implemented - Linked to WIP tab for now
            hideAllTabs();
            MainTabs.TabPages.Add(Tab_WIP);
            MainTabs.SelectedTab = Tab_WIP;
        }

        private void LicenseButton_Click(object sender, EventArgs e)
        {
            showTextTab(Properties.Resources.LICENSE, "GPLv3");
        }

        private void CreditsButton_Click(object sender, EventArgs e)
        {
            showTextTab(Properties.Resources.CREDITS, "Credits");
        }

        private void GroupLookup_Click(object sender, EventArgs e)
        {
            MainInput.Text = GroupsList.SelectedItem.ToString();
            doSearch(MainInput.Text);
        }

        private void MainInput_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (String.Empty != MainInput.Text)
                doSearch(MainInput.Text);
        }

    }
}
